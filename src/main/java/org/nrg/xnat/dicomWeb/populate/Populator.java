package org.nrg.xnat.dicomWeb.populate;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.io.DicomInputStream;
import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstance;
import org.nrg.xnatClient.XNATClient;

import java.io.File;
import java.io.IOException;

public class Populator {
    private XNATClient xnatClient;
    private DBPopulator dbPopulator;

    public Populator(XNATClient xnatClient, DBPopulator dbPopulator) {
        this.xnatClient = xnatClient;
        this.dbPopulator = dbPopulator;
    }

    public void process( ProcessInstance processInstance) throws IOException {
        try (DicomInputStream dis = new DicomInputStream( new File( processInstance.getFilePath()))) {
            Attributes dataset = dis.readDataset();

            DicomEditor editor = new DicomEditor();
            editor.addEdit( editor.new AssignValueOfTag( 0x00100020, 0x00100010));
//            editor.edit( dataset);

            dbPopulator.populate( dataset);
        }
    }

}
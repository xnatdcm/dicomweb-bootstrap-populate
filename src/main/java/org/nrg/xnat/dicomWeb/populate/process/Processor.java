package org.nrg.xnat.dicomWeb.populate.process;

import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstance;

import java.io.IOException;
import java.util.List;

public interface Processor {
    List<ProcessInstance> scanForWork() throws IOException;

    void work( ProcessInstance work) throws IOException;
}

package org.nrg.xnat.dicomWeb.populate.entities;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

@Repository
public class DicomFrameRepository extends AbstractHibernateDAO< DicomFrame> {
}

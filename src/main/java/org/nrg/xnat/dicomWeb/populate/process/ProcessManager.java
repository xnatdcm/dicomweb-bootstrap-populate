package org.nrg.xnat.dicomWeb.populate.process;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstance;
import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Coordinate the workflow. Delegates to a Processor for function. Tracks Percent remaining and estimated time remaining.
 *
 */
@Slf4j
@Component
public class ProcessManager {
    @Autowired
    private ProcessInstanceRepository piRepository;
    @Autowired
    private Processor processor;

    @Transactional
    public List<ProcessInstance> scanForWork() throws IOException {
        return processor.scanForWork();
    }

//    @Transactional
    public void work( ProcessInstance pi) throws IOException {
        pi.setStartTimestamp( new Date());

        processor.work( pi);

        pi.setStopTimestamp( new Date());
        pi.setProcessed( true);
        piRepository.saveOrUpdate(pi);
    }

    @Transactional
    public void work( List<ProcessInstance> workList) throws IOException {
        int nItems = workList.size();
        int nItemsProcessed = 0;
        LocalDateTime startTime = LocalDateTime.now();

        for( ProcessInstance pi: workList) {

            work( pi);

            nItemsProcessed++;

            if( logProgress( nItems, nItemsProcessed)) {
                LocalDateTime currentTime = LocalDateTime.now();
                long elapsedTimeSeconds = Duration.between(startTime, currentTime).getSeconds();
                float processRate = (elapsedTimeSeconds == 0) ? 0.0f : nItemsProcessed / (float) elapsedTimeSeconds;
                int nItemsRemaining = nItems - nItemsProcessed;
                long timeRemainingSeconds = (processRate != 0) ? (long) (nItemsRemaining / processRate) : -1l;
                log.info( String.format("%d items processed in %s.", nItemsProcessed, formatDurationSeconds(elapsedTimeSeconds)));
                log.info( String.format("Estimated time remaining: %d items in %s.", nItemsRemaining, formatDurationSeconds(timeRemainingSeconds)));
            }
        }
    }

    private boolean logProgress( int nItems, int nItemsProcessed) {
        return (nItemsProcessed % 1000) == 0 || (nItemsProcessed == nItems);
    }

    public String formatDurationSeconds( long seconds) {
        String format;
        if( seconds < 0) {
            format = "unavailable";
        }
        else {
            long days = seconds / (60 * 60 * 24);
            seconds = seconds - (days * 60 * 60 * 24);
            long hours = seconds / (60 * 60);
            seconds = seconds - (hours * 60 * 60);
            long minutes = seconds / 60;
            seconds = seconds - (minutes * 60);
            if (days != 0) {
                format = String.format("%d %d:%d:%d", days, hours, minutes, seconds);
            } else {
                format = String.format("%d:%d:%d", hours, minutes, seconds);
            }
        }
        return format;
    }

}

package org.nrg.xnat.dicomWeb.populate.entities.process;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstance;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class ProcessInstanceRepository extends AbstractHibernateDAO<ProcessInstance> {

    @Override
    public List<ProcessInstance> findAllByExample(ProcessInstance exampleInstance, String[] excludeProperty) {

        List<ProcessInstance> list = super.findAllByExample(exampleInstance, excludeProperty);
        return list;
    }


    /**
     * A ProcessingInstance is incomplete if is not in the table or isProcessed is true.
     *
     * @param pi
     * @return
     */
    public boolean isIncomplete( ProcessInstance pi) {
        Map<String, Object> properties = new HashMap<>();
        properties.put( "projectLabel", pi.getProjectLabel());
        properties.put( "subjectLabel", pi.getSubjectLabel());
        properties.put( "sessionLabel", pi.getSessionLabel());
        properties.put( "scanID", pi.getScanID());
        properties.put( "filePath", pi.getFilePath());
        List<ProcessInstance> matchingPI = findByProperties( properties);

        return ( matchingPI == null || matchingPI.isEmpty() || ( ! matchingPI.stream().anyMatch(ProcessInstance::isProcessed)));
    }
}

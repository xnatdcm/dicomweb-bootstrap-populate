package org.nrg.xnat.dicomWeb.populate;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstance;
import org.nrg.xnat.dicomWeb.populate.process.ProcessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SpringBootApplication (
		exclude = {HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class}
)
public class Application {
	@Autowired
	private ProcessManager processManager;
	@Autowired
	private ApplicationContext ctx;

	@Bean
	public CommandLineRunner run( ) {
		return args -> {
			System.out.println( ctx);
			System.out.println( processManager);

			String projectLabel = null;

			System.out.println("Parsing command line: " + Arrays.stream(args).collect( Collectors.joining(" ")));

			int iarg = 0;
			while( iarg < args.length) {
				switch (args[iarg].toLowerCase()) {
					case "--projectlabel":
						projectLabel = nextArg( args, ++iarg);
						if( projectLabel == null) {
							String msg = "Missing projectLabel argument for flag --projectLabel";
							System.out.println(msg);
							System.exit(1);
						}
						iarg++;
						break;
					default:
						System.out.println("Unknown argument: " + args[iarg]);
						printUsage();
						System.exit(1);
				}
			}

			try {
				List<ProcessInstance> worklist = processManager.scanForWork();
				processManager.work( worklist);

				log.info("Success.");
				System.exit(0);
			}
			catch (Exception e) {
				log.error("Failed.", e);
				System.exit(-1);
			}
		};
	}

	private String nextArg( String[] args, int iarg) {
		return (iarg < args.length)? args[iarg]: null;
	}

	private void printUsage() {
		System.out.println("dicomWeb-bootstrap-populate [--projectLabel <projectLabel>] ");
		System.out.println();
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication( Application.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

}

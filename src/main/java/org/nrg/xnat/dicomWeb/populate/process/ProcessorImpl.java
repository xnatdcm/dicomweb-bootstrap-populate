package org.nrg.xnat.dicomWeb.populate.process;

import lombok.extern.slf4j.Slf4j;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.io.DicomInputStream;
import org.nrg.xnat.datamodel.*;
import org.nrg.xnat.dicomWeb.populate.DBPopulator;
import org.nrg.xnat.dicomWeb.populate.DicomEditor;
import org.nrg.xnat.dicomWeb.populate.Populator;
import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstance;
import org.nrg.xnat.dicomWeb.populate.entities.process.ProcessInstanceRepository;
import org.nrg.xnatClient.XNATClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Coordinate the workflow. Delegates to a Processor for function. Tracks Percent remaining and estimated time remaining.
 *
 */
@Slf4j
@Component
public class ProcessorImpl implements Processor {
    @Autowired
    private XNATClient xnatClient;
    @Autowired
    private ProcessInstanceRepository piRepository;
    @Autowired
    private DBPopulator dbPopulator;

    public List<ProcessInstance> scanForWork() throws IOException {
        List<ProcessInstance> workList = new ArrayList<>();

        List<String> projectLabels = xnatClient.getProjectLabels();

        for( String projectLabel: projectLabels) {
            List<String> subjectLabels = xnatClient.getSubjectLabels( projectLabel);

            for( String subjectLabel: subjectLabels) {
                SubjectData subjectData = xnatClient.getSubject( projectLabel, subjectLabel);

                List<SubjectAssessorData> experiments = subjectData.getExperiments().getExperiment();
                List<ImageSessionData> imageSessionData = experiments.stream()
                        .filter(ImageSessionData.class::isInstance)
                        .map(ImageSessionData.class::cast)
                        .collect(Collectors.toList());

                for( ImageSessionData sessionData: imageSessionData) {
                    List<ImageScanData> imageScanData = sessionData.getScans().getScan();

                    for( ImageScanData scanData: imageScanData) {
                        Optional<ResourceCatalog> catalog = scanData.getFile().stream()
                                .filter(ResourceCatalog.class::isInstance)
                                .map( ResourceCatalog.class::cast)
                                .findAny();
                        if( catalog.isPresent()) {
                            Path catalogFilePath = Paths.get( catalog.get().getURI());
                            Path dicomDirPath = catalogFilePath.getParent();

                            List<File> files = Files.list(dicomDirPath)
                                    .filter(Files::isRegularFile)
                                    .filter(path -> path.toString().endsWith(".dcm"))
                                    .map(Path::toFile)
                                    .collect(Collectors.toList());

                            for (File file : files) {
                                ProcessInstance pi = new ProcessInstance();
                                pi.setProjectLabel(sessionData.getProject());
                                pi.setSubjectLabel(subjectLabel);
                                pi.setSessionLabel(sessionData.getLabel());
                                pi.setScanID(scanData.getID());
                                pi.setFilePath(file.getPath());

                                if( piRepository.isIncomplete( pi)) {
                                    workList.add( pi);
                                }
                            }
                        }
                    }
                }
            }
        }
        return workList;
    }

    public void work( ProcessInstance pi) throws IOException {

        try (DicomInputStream dis = new DicomInputStream( new File( pi.getFilePath()))) {
            Attributes dataset = dis.readDataset();

//            DicomEditor editor = new DicomEditor();
//            editor.addEdit( editor.new AssignValueOfTag( 0x00100020, 0x00100010));
//            editor.edit( dataset);

            dbPopulator.populate( dataset);
        }
    }

}

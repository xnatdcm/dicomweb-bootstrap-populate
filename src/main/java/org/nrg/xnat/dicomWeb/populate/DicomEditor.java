package org.nrg.xnat.dicomWeb.populate;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.VR;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DicomEditor {
    List<DicomEdit> edits;

    public DicomEditor() {
        this.edits = new ArrayList<>();
    }

    public void addEdit( DicomEdit edit) {
        edits.add( edit);
    }

    public void edit( Attributes dataset) throws IOException {
        edits.forEach( e -> {e.apply( dataset);});
    }

    public abstract interface DicomEdit {
        void apply( Attributes dataset);
    }

    public class AssignString implements DicomEdit {
        int ltag;
        String s;
        public AssignString( int ltag, String s) {
            this.ltag = ltag;
            this.s = s;
        }

        public void apply( Attributes dataset) {
            dataset.setString( ltag, VR.valueOf( ltag), s);
        }
    }
    public class AssignValueOfTag implements DicomEdit {
        int ltag;
        int rtag;
        public AssignValueOfTag( int ltag, int rtag) {
            this.ltag = ltag;
            this.rtag = rtag;
        }

        public void apply( Attributes dataset) {
            dataset.setString( ltag, VR.valueOf( ltag), dataset.getStrings(rtag));
        }
    }
}

package org.nrg.xnat.dicomWeb.populate;

import org.dcm4che3.data.Attributes;
import org.nrg.xnat.dicomWeb.populate.entities.DicomFrame;
import org.nrg.xnat.dicomWeb.populate.entities.DicomFrameRepository;
import org.nrg.xnat.dicomWeb.populate.entities.DicomInstance;
import org.nrg.xnat.dicomWeb.populate.entities.DicomInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class DBPopulator {
    @Autowired
    private DicomInstanceRepository dicomInstanceRepository;
    @Autowired
    private DicomFrameRepository dicomFrameRepository;

//    public DBPopulator( DicomInstanceRepository dicomInstanceRepository, DicomFrameRepository dicomFrameRepository) {
//        this.dicomInstanceRepository = dicomInstanceRepository;
//        this.dicomFrameRepository = dicomFrameRepository;
//    }

    public void populate( Attributes dataset) {
        DicomInstance dicomInstance = new DicomInstance(
                dataset.getString( SOPInstanceUID),
                dataset.getString( SOPClassUID),
                dataset.getInt( InstanceNumber, 1),
                dataset.getInt( Rows, 0),
                dataset.getInt( Cols, 0),
                dataset.getInt( BitsAllocated, 0),
                dataset.getInt( NumberOfFrames, 1));
        DicomFrame dicomFrame = new DicomFrame(
                dataset.getInt(    FrameNumber, 1),
                dataset.getString( ImagePositionPatient),
                dataset.getString( ImageOrientationPatient),
                dataset.getString( PixelSpacing),
                dataset.getString( FrameOfReferenceUid));

        dicomInstanceRepository.create( dicomInstance);
        dicomFrameRepository.create( dicomFrame);
    }

    private static int SOPInstanceUID = 0x00080018;
    private static int SOPClassUID = 0x00080016;
    private static int InstanceNumber = 0x00200013;
    private static int Rows = 0x00280010;
    private static int Cols = 0x00280011;
    private static int BitsAllocated = 0x00280100;
    private static int NumberOfFrames = 0x00280008;
    private static int FrameNumber;
    private static int ImageOrientationPatient = 0x0020037;
    private static int ImagePositionPatient = 0x0020032;
    private static int PixelSpacing = 0x00280030;
    private static int FrameOfReferenceUid = 0x00200052;
}

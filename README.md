# DICOMweb-bootstrap-populate #

This tool is an aide to bootstrap DICOMweb functionality into existing XNAT instances. DICOMweb in XNAT requires 
populating new database columns as well as updating existing columns. This is a stand-alone application, meant to be
run once by an administrator prior to updating an existing XNAT instance to a version that is DICOMweb enabled.

### Version ###

This code has been tested with the following versions.

Version        | Date | From XNAT version | To XNAT Version
---------------|------|------------|---------
1.0.0-SNAPSHOT | 2021-04-03 | 1.8.0, 1.8.1      | 1.8.2

### Usage ###

The jar file is executable.

    java -jar dicomweb-bootstrap-populate.jar [--projectLabel <projectLabel>]

The default is to run on all projects in the archive. Execution can optionally by restricted to a single project by
supplying the project's label.

* ***WARNING:*** This will modify the contents of the XNAT database. Only run this command when it has been configured
  for all projects in the archive, and you are certain that these changes will not affect the usability of your data. The
  changes are designed to not affect existing functionality in un-modified instances of XNAT. Modified versions of XNAT 
  may have different requirements. Details of database changes are documented in 
  (***TODO***) [DICOMweb Bootstrap Database Modifications][https://wiki.nrg.org]
* ***WARNING:*** Version 1.0.0-SNAPSHOT is under development and will only work on the MIRRIR XNAT instance. MIRRIR has 
  a unique project structure and in particular, has a single configuration that applies to all projects. Future versions
  of this application must provide a means to provide different mappings for different projects (***TODO***).
* This application extracts values from existing DICOM data in the archive. It must be run on a server that has
access to this filesystem.
  
### Configuration ###

* All configuration is done in the file `application.properties` as part of the jar file. (***TODO***) Make this configuration
  more accessible to the user.
  * XNAT host coordinates and admin account credentials
  * XNAT database coordinates and admin account credentials
* Per-project value mappings must be implemented and exposed. (***TODO***)

### Download ###

The jar file may be downloaded from the Download tab of this repository. (***TODO***)

### Build From Source ###

* Clone this repository.
* `mvn clean install`